package com.example.idnis.idnisconsultorias;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void TelaEmpresa(View view){
        Intent tela = new Intent(this, Empresa.class);
        startActivity(tela);
    }

    public void TelaServico(View view) {
        Intent tela1 = new Intent(this, Servico.class);
        startActivity(tela1);
    }

    public void TelaCliente(View view) {
        Intent tela2 = new Intent(this, Cliente.class);
        startActivity(tela2);
    }

    public void TelaContato(View view) {
        Intent tela3 = new Intent(this, Contato.class);
        startActivity(tela3);
    }
}
